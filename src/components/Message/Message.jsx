import React from 'react';
import { getDateTime } from '../../helpers';
import PropTypes from 'prop-types';

import classes from './Message.module.css';

export const Message = ({message, userData, deleteMessage, setEditMessage, likeMessage}) => {
  const {text, avatar, user, createdAt, id, likes, userId} = message;

  const time = getDateTime(new Date(createdAt), true);

  const likesClasses = [classes.like];
  if (likes.includes(userData.id)) {
    likesClasses.push(classes.active)
  }

  const cls = [classes.MessageBlock];
  if (userId === userData.id) {
    cls.push(classes.own);
  }

  const onDelete = () => {
    deleteMessage(id)
  }

  const onEdit = () => {
    setEditMessage(id, text);
  }

  const onLike = () => {
    if (userId === userData.id) return;
    likeMessage(id);
  }

  return (
    <div className={cls.join(' ')}>
      {
        userData.id !== userId && (
          <div className={classes.authorBlock}>
            <div className={classes.img}>
              <img src={avatar} alt="avatar"/>
            </div>
            <div className={classes.username}>{user}</div>
          </div>
        )
      }
      <div className={classes.message}>
        <pre>{text}</pre>
        <div className={classes.toolbar}>
          <i
            className="fas fa-edit fa-xs"
            onClick={() => onEdit()}
          />
          <i
            className="fas fa-trash fa-xs"
            onClick={() => onDelete()}
          />
        </div>
        <div className={classes.time}>{time}</div>
        <div className={likesClasses.join(' ')}>
          <div>{likes.length}</div>
          <i
            className="fas fa-thumbs-up fa-lg"
            onClick={() => onLike()}
          />
        </div>
      </div>
    </div>
  )
}

Message.propTypes = {
  message: PropTypes.objectOf(PropTypes.any),
  userData: PropTypes.objectOf(PropTypes.string),
  deleteMessage: PropTypes.func,
  setEditMessage: PropTypes.func,
  likeMessage: PropTypes.func
}
