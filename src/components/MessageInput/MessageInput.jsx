import React from 'react';
import classes from './MessageInput.module.css';
import PropTypes from 'prop-types';

export class MessageInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: ''
    }
  }

  onChangeHandler = e => {
    const value = e.target.value;

    this.setState({text: value})
  }

  onSendHandler = () => {
    if (!this.state.text.trim()) return;

    this.props.sendMessage(this.state.text, this.props.user);
    this.setState({text: ''});
  }


  render() {
    return (
      <div className={classes.MessageInput}>
      <textarea
        placeholder='Write something'
        rows={1}
        value={this.state.text}
        onChange={this.onChangeHandler}
      />
        <button
          type='submit'
          onClick={this.onSendHandler}
        >
          Send
        </button>
      </div>
    )
  }
}

MessageInput.propTypes = {
  user: PropTypes.objectOf(PropTypes.string),
  sendMessage: PropTypes.func
}
