import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { editMessageDone } from '../../actions/chatActions';
import Backdrop from '../UI/Backdrop/Backdrop';
import { dropEditMessage } from '../../actions/editActions';

import classes from './EditMessageModal.module.css';

class EditMessageModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: ''
    }
  }

  onChangeHandler = e => {
    const value = e.target.value;

    this.setState({text: value});
  }

  onEditDone = text => {
    if (!text.trim()) return;

    this.props.dropEditMessage();
    this.props.editMessageDone(text, this.props.editMessageId);
  }

  onClose = () => {
    this.props.dropEditMessage();
  }

  moveCaretAtEnd = e => {
    const value = e.target.value
    e.target.value = ''
    e.target.value = value
  }

  getModalContent = () => {
    return (
      <>
        <Backdrop onClick={this.onClose}/>
        <div className={classes.EditMessage}>
          <h4>Edit message</h4>
          <button
            className={classes.btnClose}
            onClick={this.onClose}
          >
            <span>&times;</span>
          </button>
          <textarea
            autoFocus
            onFocus={this.moveCaretAtEnd}
            value={this.state.text}
            rows={3}
            onChange={this.onChangeHandler} />
          <button
            className={classes.btnEdit}
            onClick={() => this.onEditDone(this.state.text)}
          >
            Save
          </button>
        </div>
      </>
    )
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.text !== this.props.text) {
      this.setState({text: nextProps.text});
    }
  }

  render() {
    return this.props.editMessageId ? this.getModalContent() : null
  }
}

EditMessageModal.propTypes = {
  text: PropTypes.string,
  editMessageId: PropTypes.string,
  dropEditMessage: PropTypes.func,
  editMessageDone: PropTypes.func
}

const mapStateToProps = state => ({
  editMessageId: state.edit.editMessageId,
  text: state.edit.text
})

const mapDispatchToProps = dispatch => ({
  dropEditMessage: () => dispatch(dropEditMessage()),
  editMessageDone: (text, id) => dispatch(editMessageDone(text, id))
})

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageModal)
