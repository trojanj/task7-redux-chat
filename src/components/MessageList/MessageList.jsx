import React from 'react';
import { Message } from '../Message/Message';
import { Timeline } from '../Timeline/Timeline';
import { passedDaysFromDate } from '../../helpers';
import PropTypes from 'prop-types';

import classes from './MessageList.module.css';

export class MessageList extends React.Component {
  scrollToBottom = () => {
    const scrollHeight = this.messageList.scrollHeight;
    const height = this.messageList.clientHeight;
    const maxScrollTop = scrollHeight - height;
    this.messageList.scrollTop = Math.max(0, maxScrollTop);
  }

  renderTimeline = (message, previousMessage) => {
    const date = new Date(message.createdAt);
    const previousDate = new Date(previousMessage?.createdAt);
    const passedDaysFromCurrentDate = passedDaysFromDate(date);
    const passedDaysFromPreviousDate = passedDaysFromDate(previousDate);

    const checkRenderTimeline = () => {
      return !previousMessage || passedDaysFromCurrentDate !== passedDaysFromPreviousDate
    }

    if (checkRenderTimeline()) {
      return <Timeline
        passedDays={passedDaysFromCurrentDate}
        messageDate={date}
      />
    }
  }

  componentDidUpdate(prevProps) {
    const isNewMessage = () => this.props.messages.length !== prevProps.messages.length;
    const isNewMessageAddByCurrentUser = () => (
      this.props.messages[this.props.messages.length - 1].userId === this.props.user.id
    );

    if ( isNewMessage() && isNewMessageAddByCurrentUser()) {
      this.scrollToBottom();
    }
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  render() {
    const {messages, user, deleteMessage, setEditMessage, likeMessage} = this.props;

    return (
      <div
        className={classes.MessageList}
        ref={(div) => { this.messageList = div }}
      >
        { messages.map((message, ind) => (
          <React.Fragment key={message.id}>
            { this.renderTimeline(message, messages[ind - 1]) }
            <Message
              message={message}
              userData={user}
              deleteMessage={deleteMessage}
              setEditMessage={setEditMessage}
              likeMessage={likeMessage}
            />
          </React.Fragment>
        )) }
      </div>
    )
  }
}

MessageList.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object),
  user: PropTypes.objectOf(PropTypes.string),
  deleteMessage: PropTypes.func,
  setEditMessage: PropTypes.func,
  likeMessage: PropTypes.func
}

