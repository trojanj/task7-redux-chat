import {
  DROP_EDIT_MESSAGE,
  SET_EDIT_MESSAGE
} from '../constants/actionTypes';

export const setEditMessage = (id, text) => ({
  type: SET_EDIT_MESSAGE,
  id,
  text
});

export const dropEditMessage = () => ({
  type: DROP_EDIT_MESSAGE
});