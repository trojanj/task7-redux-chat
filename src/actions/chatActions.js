import {
  DELETE_MESSAGE,
  EDIT_MESSAGE_DONE,
  FETCH_MESSAGES_START,
  FETCH_MESSAGES_SUCCESS,
  LIKE_MESSAGE,
  SEND_MESSAGE
} from '../constants/actionTypes';
import { v4 as uuidv4 } from 'uuid';

export async function fetchMessages(dispatch) {
  try {
    dispatch(fetchMessagesStart());

    const response = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json');
    const messages = await response.json();
    messages
      .sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt))
      .forEach(message => {message.likes = []})

    console.log(messages)

    dispatch(fetchMessagesSuccess(messages));
  } catch (e) {
    console.log(e)
  }
}

function fetchMessagesStart() {
  return {
    type: FETCH_MESSAGES_START
  }
}

function fetchMessagesSuccess(messages) {
  return {
    type: FETCH_MESSAGES_SUCCESS,
    messages
  }
}

export function sendMessage(text, user) {
  const newMessage = {
    id: uuidv4(),
    text,
    user: user.name,
    avatar:  user.avatar,
    userId: user.id,
    editedAt: '',
    createdAt: new Date().toString(),
    likes: []
  }

  return {
    type: SEND_MESSAGE,
    newMessage
  }
}

export function deleteMessage(id) {
  return {
    type: DELETE_MESSAGE,
    id
  }
}

export function editMessageDone(text, id) {
  return {
    type: EDIT_MESSAGE_DONE,
    text,
    id
  }
}

export function likeMessage(id) {
  return {
    type: LIKE_MESSAGE,
    id
  }
}
