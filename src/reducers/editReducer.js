import { DROP_EDIT_MESSAGE, SET_EDIT_MESSAGE, } from '../constants/actionTypes';

const initialState = {
  editMessageId: null,
  text: ''
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_EDIT_MESSAGE:
      return {
        editMessageId: action.id,
        text: action.text
      }
    case DROP_EDIT_MESSAGE:
      return {
        editMessageId: null,
        text: ''
      }
    default:
      return state
  }
}
