import {
  DELETE_MESSAGE,
  EDIT_MESSAGE_DONE,
  FETCH_MESSAGES_START,
  FETCH_MESSAGES_SUCCESS,
  LIKE_MESSAGE,
  SEND_MESSAGE
} from '../constants/actionTypes';

const initialState = {
  messages: [],
  isLoading: false,
  user: {
    id: '9a233231-84c0-12e8-8g0n-8a1e646a4me7',
    name: 'Arnold',
    avatar: process.env.PUBLIC_URL + '/img/arnold-avatar.jpg'
  },
  chatName: 'My chat'
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGES_START:
      return {
        ...state, isLoading: true
      }
    case FETCH_MESSAGES_SUCCESS:
      return {
        ...state, messages: action.messages, isLoading: false
      }
    case SEND_MESSAGE:
      return {
        ...state, messages: [...state.messages, action.newMessage]
      }
    case DELETE_MESSAGE:
      return {
        ...state, messages: state.messages.filter(message => message.id !== action.id)
      }
    case EDIT_MESSAGE_DONE: {
      const updatedMessages = state.messages.map(message => {
        if (message.id === action.id) {
          return {
            ...message,
            text: action.text,
            editedAt: new Date().toString()
          }
        } else {
          return message
        }
      })

      return {
        ...state, messages: updatedMessages
      }
    }
    case LIKE_MESSAGE: {
      const updatedMessages = state.messages.map(message => {
        if (message.id === action.id) {
          let updatedLikes;
          if (message.likes.includes(state.user.id)) {
            updatedLikes = message.likes.filter(userId => userId !== state.user.id)
          } else {
            updatedLikes = [...message.likes, state.user.id]
          }

          return { ...message, likes: updatedLikes }
        } else {
          return message
        }
      })

      return {
        ...state, messages: updatedMessages
      }
    }
    default:
      return state
  }
}
