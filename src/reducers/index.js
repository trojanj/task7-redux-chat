import { combineReducers } from 'redux';
import chat from './chatReducer';
import edit from './editReducer';

const rootReducer = combineReducers({
  chat,
  edit
});

export default rootReducer;