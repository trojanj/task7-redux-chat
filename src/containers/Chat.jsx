import React from 'react';
import { ChatHeader } from '../components/ChatHeader/ChatHeader';
import { MessageList } from '../components/MessageList/MessageList';
import { MessageInput } from '../components/MessageInput/MessageInput';
import { Loader } from '../components/UI/Loader/Loader';
import { connect } from 'react-redux';
import { deleteMessage, fetchMessages, likeMessage, sendMessage } from '../actions/chatActions';
import EditMessageModal from '../components/EditMessageModal/EditMessageModal';
import { setEditMessage } from '../actions/editActions';
import PropTypes from 'prop-types';

import classes from './Chat.module.css';

class Chat extends React.Component {
  getUsersNumber = () => {
    let users = [];

    this.props.messages.forEach(message => {
      if (!users.includes(message.userId)) {
        users.push(message.userId);
      }
    })

    return users.length;
  }

  handleKeyDown = e => {
    if (e.code === 'ArrowUp') {
      e.preventDefault();

      const lastUserMessage = [...this.props.messages]
        .reverse()
        .find(message => message.userId === this.props.user.id);

      if (!lastUserMessage) return;

      this.props.setEditMessage(lastUserMessage.id, lastUserMessage.text);
    }
  }

  componentDidMount() {
    this.props.fetchMessages();
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  render() {
    if (this.props.isLoading) {
      return <div className={classes.Chat}>
        <Loader/>
      </div>
    }

    const {
      messages, chatName, user, deleteMessage,
      setEditMessage, likeMessage, sendMessage
    } = this.props;
    const usersNumber = this.getUsersNumber();
    const lastMessageCreatedAt = messages[messages.length - 1]?.createdAt;

    return (
      <div className={classes.Chat}>
        <ChatHeader
          usersNumber={usersNumber}
          messagesNumber={messages.length}
          lastMessageCreatedAt={lastMessageCreatedAt}
          chatName={chatName}
        />
        <MessageList
          messages={messages}
          user={user}
          deleteMessage={deleteMessage}
          setEditMessage={setEditMessage}
          likeMessage={likeMessage}
        />
        <MessageInput
          sendMessage={sendMessage}
          user={user}
        />
        <EditMessageModal/>
      </div>
    )
  }
}

Chat.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object),
  isLoading: PropTypes.bool,
  user: PropTypes.objectOf(PropTypes.string),
  chatName: PropTypes.string,
  fetchMessages: PropTypes.func,
  sendMessage: PropTypes.func,
  deleteMessage: PropTypes.func,
  setEditMessage: PropTypes.func,
  likeMessage: PropTypes.func
}

const mapStateToProps = state => ({
  messages: state.chat.messages,
  isLoading: state.chat.isLoading,
  user: state.chat.user,
  chatName: state.chat.chatName
})

const mapDispatchToProps = dispatch => ({
  fetchMessages: () => fetchMessages(dispatch),
  sendMessage: (text, user) => dispatch(sendMessage(text, user)),
  deleteMessage: id => dispatch(deleteMessage(id)),
  setEditMessage: (id, text) => dispatch(setEditMessage(id, text)),
  likeMessage: id => dispatch(likeMessage(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
